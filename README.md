# Starwars Starship Data Service

This is a simple flask-based service that keeps database records
related to information about Starships from the Star Wars Universe.The
only relevant data is the ship's name and the its hyperdrive rating.


## Running the service

This repository provides a docker-compose configuration that lets you
run the service and also provides a PostgreSQL database. Please ensure
that you have docker and docker-compose installed in your systemso
that you can run `docker-compose up` to see the message about both the
`web` and the `db` services installed.

For simplicity, the web application is set up in a way that it can
only connect to the database from docker.

Upon starting, the web application will be running and listening to
requests on port 5000. You can access this port from your host system,
so if you go to any web browser, you should be able to access
`http://localhost:5000` and get a response from the service.

The service has no data when it is started, so you will need to add
some entries to the database.


## Adding data

You can add new entries representing startships by sending POST
requests in JSON format. The only required attribute is the name.

Using curl, you can add some data taken from
https://en.wikipedia.org/wiki/List_of_Star_Wars_spacecraft

```
curl -X POST -i http://localhost:5000 --data '{"name": "Naboo Royal Starship", "hyperdrive_rating": 4}'
curl -X POST -i http://localhost:5000 --data '{"name": "Republic Cruiser", "hyperdrive_rating": 4}'
curl -X POST -i http://localhost:5000 --data '{"name": "Naboo Star Skiff"}'  # no hyperdrive rating included
curl -X POST -i http://localhost:5000 --data '{"name": "Naboo Royal Cruiser", "hyperdrive_rating": 1}'
curl -X POST -i http://localhost:5000 --data '{"name": "Techno Union Starship", "hyperdrive_rating": 1}'
curl -X POST -i http://localhost:5000 --data '{"name": "Dooku'\''s solar sailer", "hyperdrive_rating": 2}'
```

The result of each call should be an HTTP response with status code
201 and the body showing the full representation of the resource,
i.e. with the id attribute.

## API Description

The service provides only two operations through the root "/" endpoint:

 - GET to obtain a list of all recorded starships, ordered by its hyperdrive rating.
 - POST to create a new record.


All payloads and responses are in JSON, and records can not have
duplicated names. Hyperdrive ratings are represented as an integer
scale from 1 to 5.


## Architectural / High-level description.

The code follows a pretty standard pattern regarding common REST
APIs. The code contained in the "starwars" module is composed of the
following files/modules:

The following libraries were used:

 - `SQLAlchemy` as Obejct-Relational Mapper
 - `marshmallow` for data/schema validation
 - `flask-restful` for definition of HTTP Resources, content negotiation and formatting


The following modules are relevant parts of the application.

- `models.py` contains the definition of the Ship as it relates to the database.
- `schemas.py` contains the definition of the Ship Schema, i.e, as it relates to the business logic.
- `resources.py` defines the Ship Resource, i.e, the representation in the REST API.
- `api.py` is the main entrypoint of the application and sets everything up for running.


## Assumptions

The main point to figure out was about how to model the concept of
hyperdrive rating. I decided to opt for a scale of integer between 1
to 5, with 5 being the highest. The endpoint lists the results ordered
by hypdrive rating, from best to worst, and those without the value
are listed last.
