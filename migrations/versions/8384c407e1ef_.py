"""empty message

Revision ID: 8384c407e1ef
Revises: 
Create Date: 2020-11-30 00:06:13.974554

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '8384c407e1ef'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('ship', 'hyperdrive_rating',
               existing_type=sa.INTEGER(),
               nullable=True)
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('ship', 'hyperdrive_rating',
               existing_type=sa.INTEGER(),
               nullable=False)
    # ### end Alembic commands ###
