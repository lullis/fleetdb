from marshmallow import fields
from marshmallow.validate import Range

from . import ma
from .models import Ship


class ShipSchema(ma.SQLAlchemySchema):
    class Meta:
        model = Ship

    id = ma.auto_field()
    name = ma.auto_field()
    hyperdrive_rating = fields.Int(required=False, validate=Range(min=1, max=5))
