from starwars import db, manager


class Ship(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), unique=True, nullable=False)
    hyperdrive_rating = db.Column(db.Integer, nullable=True)

    @classmethod
    def by_hyperdrive_rating(cls):
        cls.query.order_by()

    def __repr__(self):
        return f"<Ship {self.name}>"


if __name__ == "__main__":
    manager.run()
