from flask_restful import Api

from starwars import app, db
from starwars.resources import ShipList

api = Api(app)

api.add_resource(ShipList, "/")

if __name__ == "__main__":
    db.create_all()
    app.run(host="0.0.0.0", debug=True)
