import os

from flask import Flask
from flask_marshmallow import Marshmallow
from flask_sqlalchemy import SQLAlchemy
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand

app = Flask(__name__)

DB_NAME = os.getenv("POSTGRES_DB")
DB_USER = os.getenv("POSTGRES_USER")
DB_PASS = os.getenv("POSTGRES_PASSWORD")
DB_HOST = os.getenv("POSTGRES_HOST", "db")
DB_PORT = os.getenv("POSTGRES_PORT", 5432)
DB_URI = f"postgres://{DB_USER}:{DB_PASS}@{DB_HOST}:{DB_PORT}/{DB_NAME}"


def create_db(application, connection_uri):
    application.config["SQLALCHEMY_DATABASE_URI"] = connection_uri
    return SQLAlchemy(application)


def init_db(database):
    database.create_all()


ma = Marshmallow(app)
db = create_db(app, DB_URI)
migrate = Migrate(app, db)
manager = Manager(app)
manager.add_command("db", MigrateCommand)
