from flask import request
from flask_restful import Resource
from marshmallow.exceptions import ValidationError

from . import db
from .models import Ship
from .schemas import ShipSchema


ships_schema = ShipSchema(many=True)
ship_schema = ShipSchema()


class ShipList(Resource):
    def get(self):
        ships = Ship.query.order_by(Ship.hyperdrive_rating.desc().nullslast())
        return ships_schema.dump(ships), 200

    def post(self):
        payload = request.get_json(force=True)
        if not payload:
            return {"error": "No data provided"}, 400

        try:
            ship_data = ship_schema.load(payload)
        except ValidationError as error:
            return error.messages, 422

        ship_name = ship_data["name"]
        if Ship.query.filter_by(name=ship_name).first():
            return {"error": f"{ship_name} is already registered"}, 400

        ship = Ship(**ship_data)
        db.session.add(ship)
        db.session.commit()

        return ship_schema.dump(ship), 201
