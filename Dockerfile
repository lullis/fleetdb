# Start with a Python image.
FROM python:latest

ENV PYTHONUNBUFFERED 1

# Install some necessary things.
RUN apt-get update
RUN apt-get install -y netcat

# Copy all relevant files into the image.
COPY . /app
WORKDIR /app

# Install our requirements.
RUN pip install --no-cache-dir -r /app/requirements.txt
RUN pip install --no-cache-dir -e /app


CMD ["python", "-m", "starwars.api"]
